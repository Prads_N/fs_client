﻿namespace FinalSymphonyClient.Tests.Models
{
    public class SimpleEventModel
    {
        public ulong Id { get; set; }
        public int Age { get; set; }
        public string Name { get; set; }
        public double Salary { get; set; }
    }
}