﻿namespace FinalSymphonyClient.Tests.Models
{
    using System;
    using System.Collections.Generic;

    public class ComplexEventModel
    {
        public Guid Id { get; set; }
        public DateTime DateCreated { get; set; }
        public Status Status { get; set; }
        public IEnumerable<bool> Booleans { get; set; }
        public TimeSpan[] TimeSpans { get; set; }
        public Child1 FirstChild { get; set; }
        public Child2 SecondChild { get; set; }
    }

    public enum Status
    {
        Active = 1,
        Inactive = 2
    }

    public class Child1
    {
        public List<string> Name { get; set; }
        public DateTimeOffset Offset { get; set; }
    }

    public class Child2
    {
        public ulong? Id1 { get; set; }
        public ulong? Id2 { get; set; }
        public Child1 Child1 { get; set; }
    }
}
