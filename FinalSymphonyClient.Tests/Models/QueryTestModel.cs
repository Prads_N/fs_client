﻿namespace FinalSymphonyClient.Tests.Models
{
    using System;
    using System.Collections.Generic;

    internal class QueryTestModel
    {
        public bool Checked { get; set; }
        public byte Status { get; set; }
        public char Letter { get; set; }
        public short Small { get; set; }
        public ushort USmall { get; set; }
        public int ThirtyTwo { get; set; }
        public uint UThirtyTwo { get; set; }
        public long SixtyFour { get; set; }
        public ulong USixtyFour { get; set; }
        public float ICanFloat { get; set; }
        public double ICanFloatEvenBigger { get; set; }
        public string Name { get; set; }
        public bool? NullMe { get; set; }

        public Guid Gid { get; set; }
        public TimeSpan Offset { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTimeOffset DateCreatedOffset { get; set; }
        public Type VType { get; set; }

        public ulong[] UArray { get; set; }
        public IEnumerable<double> DEnumerable { get; set; }

        public QueryTestModel Myself { get; set; }
    }

    internal enum Type
    {
        Volatile,
        NonVolatile
    }
}