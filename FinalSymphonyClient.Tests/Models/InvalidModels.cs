﻿using System.Collections.Generic;

namespace FinalSymphonyClient.Tests.Models
{
    public class InvalidModel1
    {
        public InvalidModel1 [] Models { get; set; }
    }

    public class InvalidModel2
    {
        public List<InvalidModel1> Model { get; set; }
    }
}