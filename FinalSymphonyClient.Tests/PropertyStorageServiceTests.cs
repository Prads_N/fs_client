﻿namespace FinalSymphonyClient.Tests
{
    using global::FinalSymphonyClient.Exceptions;
    using global::FinalSymphonyClient.Models.Internal;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Tests.Models;
    using Xunit;

    public class PropertyStorageServiceTests
    {
        [Fact]
        public void Can_Process_Simple_Model()
        {
            var simpleObject = new SimpleEventModel
            {
                Id = 1234,
                Age = 24,
                Name = "Jack",
                Salary = 250000.56
            };

            var result = PropertyStorageService.GetPropertyData("", typeof(SimpleEventModel).GetProperties(), simpleObject).ToList();

            Assert.Equal(4, result.Count);

            Assert.Equal("Id", result[0].PropertyName);
            Assert.Equal(1234UL, result[0].Value);
            Assert.Equal(ValueDataType.UnsignedInteger, result[0].ValueDataType);

            Assert.Equal("Age", result[1].PropertyName);
            Assert.Equal(24, result[1].Value);
            Assert.Equal(ValueDataType.SignedInteger, result[1].ValueDataType);

            Assert.Equal("Name", result[2].PropertyName);
            Assert.Equal("Jack", result[2].Value);
            Assert.Equal(ValueDataType.Keyword, result[2].ValueDataType);

            Assert.Equal("Salary", result[3].PropertyName);
            Assert.Equal(250000.56D, result[3].Value);
            Assert.Equal(ValueDataType.Double, result[3].ValueDataType);
        }

        [Fact]
        public void Can_Process_Complex_Model()
        {
            var guid = Guid.NewGuid();

            var complexModel = new ComplexEventModel
            {
                Booleans = new[] { true, false, false, true },
                DateCreated = new DateTime(1970, 1, 1, 0, 0, 0, 1, DateTimeKind.Utc),
                FirstChild = new Child1
                {
                    Name = new List<string> { "Jill", "Bill" },
                    Offset = new DateTimeOffset(new DateTime(1970, 1, 1, 0, 0, 0, 2, DateTimeKind.Utc))
                },
                Id = guid,
                SecondChild = new Child2
                {
                    Id1 = 100,
                    Id2 = null,
                    Child1 = new Child1
                    {
                        Name = new List<string> { "Jack", "Wack" },
                        Offset = new DateTimeOffset(new DateTime(1970, 1, 1, 0, 0, 0, 599, DateTimeKind.Utc))
                    }
                },
                Status = Status.Inactive,
                TimeSpans = new[] { new TimeSpan(0, 0, 0, 1, 0), new TimeSpan(0, 0, 0, 2, 500) }
            };

            var result = PropertyStorageService.GetPropertyData("", typeof(ComplexEventModel).GetProperties(), complexModel).ToList();

            Assert.Equal(15, result.Count);

            Assert.Equal("Id", result[0].PropertyName);
            Assert.Equal(guid.ToString(), result[0].Value);
            Assert.Equal(ValueDataType.Keyword, result[0].ValueDataType);

            Assert.Equal("DateCreated", result[1].PropertyName);
            Assert.Equal(1.0D, result[1].Value);
            Assert.Equal(ValueDataType.Double, result[1].ValueDataType);

            Assert.Equal("Status", result[2].PropertyName);
            Assert.Equal("Inactive", result[2].Value);
            Assert.Equal(ValueDataType.Keyword, result[2].ValueDataType);

            Assert.Equal("Booleans", result[3].PropertyName);
            Assert.Equal(true, result[3].Value);
            Assert.Equal(ValueDataType.UnsignedInteger, result[3].ValueDataType);

            Assert.Equal("Booleans", result[4].PropertyName);
            Assert.Equal(false, result[4].Value);
            Assert.Equal(ValueDataType.UnsignedInteger, result[4].ValueDataType);

            Assert.Equal("TimeSpans", result[5].PropertyName);
            Assert.Equal(1000.0D, result[5].Value);
            Assert.Equal(ValueDataType.Double, result[5].ValueDataType);

            Assert.Equal("TimeSpans", result[6].PropertyName);
            Assert.Equal(2500.0D, result[6].Value);
            Assert.Equal(ValueDataType.Double, result[6].ValueDataType);

            Assert.Equal("FirstChild.Name", result[7].PropertyName);
            Assert.Equal("Jill", result[7].Value);
            Assert.Equal(ValueDataType.Keyword, result[7].ValueDataType);

            Assert.Equal("FirstChild.Name", result[8].PropertyName);
            Assert.Equal("Bill", result[8].Value);
            Assert.Equal(ValueDataType.Keyword, result[8].ValueDataType);

            Assert.Equal("FirstChild.Offset", result[9].PropertyName);
            Assert.Equal(2L, result[9].Value);
            Assert.Equal(ValueDataType.SignedInteger, result[9].ValueDataType);

            Assert.Equal("SecondChild.Id1", result[10].PropertyName);
            Assert.Equal(100UL, result[10].Value);
            Assert.Equal(ValueDataType.UnsignedInteger, result[10].ValueDataType);

            Assert.Equal("SecondChild.Id2", result[11].PropertyName);
            Assert.Null(result[11].Value);
            Assert.Equal(ValueDataType.UnsignedInteger, result[11].ValueDataType);

            Assert.Equal("SecondChild.Child1.Name", result[12].PropertyName);
            Assert.Equal("Jack", result[12].Value);
            Assert.Equal(ValueDataType.Keyword, result[12].ValueDataType);

            Assert.Equal("SecondChild.Child1.Name", result[13].PropertyName);
            Assert.Equal("Wack", result[13].Value);
            Assert.Equal(ValueDataType.Keyword, result[13].ValueDataType);

            Assert.Equal("SecondChild.Child1.Offset", result[14].PropertyName);
            Assert.Equal(599L, result[14].Value);
            Assert.Equal(ValueDataType.SignedInteger, result[14].ValueDataType);
        }

        [Fact]
        public void Throws_Excepotion_For_Invalid_Models()
        {
            var invalidModel1 = new InvalidModel1
            {
                Models = null
            };

            var invalidModel2 = new InvalidModel2
            {
                Model = null
            };

            Assert.Throws<FinalSymphonyException>(
                () => PropertyStorageService.GetPropertyData(
                    "", typeof(InvalidModel1).GetProperties(), invalidModel1));

            Assert.Throws<FinalSymphonyException>(
                () => PropertyStorageService.GetPropertyData(
                    "", typeof(InvalidModel2).GetProperties(), invalidModel2));
        }
    }
}