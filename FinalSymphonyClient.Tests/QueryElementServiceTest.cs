﻿namespace FinalSymphonyClient.Tests
{
    using global::FinalSymphonyClient.Models.Internal;
    using global::FinalSymphonyClient.Models;
    using Models;
    using Xunit;
    using System;
    using global::FinalSymphonyClient.Exceptions;

    public class QueryElementServiceTest
    {
        [Fact]
        public void Can_Proccess_Valid_Properties()
        {
            var element = QueryElementService.GetQueryElement<QueryTestModel>(qt => qt.Checked, QueryOperator.Equals, false);
            Assert.Equal("Checked", element.Property);
            Assert.Equal(QueryOperator.Equals, element.Operator);
            Assert.Equal("0", element.Value);
            Assert.Equal(ValueDataType.UnsignedInteger, element.ValueDataType);

            element = QueryElementService.GetQueryElement<QueryTestModel>(qt => qt.DateCreated, QueryOperator.GreaterThanOrEqual, 
                new DateTime(1970, 1, 1, 0, 0, 0, 60, DateTimeKind.Utc));
            Assert.Equal("DateCreated", element.Property);
            Assert.Equal(QueryOperator.GreaterThanOrEqual, element.Operator);
            Assert.Equal("60", element.Value);
            Assert.Equal(ValueDataType.Double, element.ValueDataType);

            element = QueryElementService.GetQueryElement<QueryTestModel>(qt => qt.DateCreatedOffset, QueryOperator.LessThanOrEqual, 
                new DateTimeOffset(new DateTime(1970, 1, 1, 0, 0, 1, 0, DateTimeKind.Utc)));
            Assert.Equal("DateCreatedOffset", element.Property);
            Assert.Equal(QueryOperator.LessThanOrEqual, element.Operator);
            Assert.Equal("1000", element.Value);
            Assert.Equal(ValueDataType.SignedInteger, element.ValueDataType);

            element = QueryElementService.GetQueryElement<QueryTestModel>(qt => qt.DEnumerable, QueryOperator.GreaterThan, 30.1D);
            Assert.Equal("DEnumerable", element.Property);
            Assert.Equal(QueryOperator.GreaterThan, element.Operator);
            Assert.Equal(30.1D.ToString(), element.Value);
            Assert.Equal(ValueDataType.Double, element.ValueDataType);

            var guid = Guid.NewGuid();
            element = QueryElementService.GetQueryElement<QueryTestModel>(qt => qt.Gid, QueryOperator.Equals, guid);
            Assert.Equal("Gid", element.Property);
            Assert.Equal(QueryOperator.Equals, element.Operator);
            Assert.Equal(guid.ToString(), element.Value);
            Assert.Equal(ValueDataType.Keyword, element.ValueDataType);

            element = QueryElementService.GetQueryElement<QueryTestModel>(qt => qt.ICanFloat, QueryOperator.LessThan, 2.3F);
            Assert.Equal("ICanFloat", element.Property);
            Assert.Equal(QueryOperator.LessThan, element.Operator);
            Assert.Equal(2.3F.ToString(), element.Value);
            Assert.Equal(ValueDataType.Double, element.ValueDataType);

            element = QueryElementService.GetQueryElement<QueryTestModel>(qt => qt.ICanFloatEvenBigger, QueryOperator.NotEqual, 233.22555D);
            Assert.Equal("ICanFloatEvenBigger", element.Property);
            Assert.Equal(QueryOperator.NotEqual, element.Operator);
            Assert.Equal(233.22555D.ToString().ToString(), element.Value);
            Assert.Equal(ValueDataType.Double, element.ValueDataType);

            element = QueryElementService.GetQueryElement<QueryTestModel>(qt => qt.Letter, QueryOperator.Equals, 'b');
            Assert.Equal("Letter", element.Property);
            Assert.Equal(QueryOperator.Equals, element.Operator);
            Assert.Equal('b'.ToString(), element.Value);
            Assert.Equal(ValueDataType.Keyword, element.ValueDataType);

            element = QueryElementService.GetQueryElement<QueryTestModel>(qt => qt.Name, QueryOperator.Equals, "Final");
            Assert.Equal("Name", element.Property);
            Assert.Equal(QueryOperator.Equals, element.Operator);
            Assert.Equal("Final", element.Value);
            Assert.Equal(ValueDataType.Keyword, element.ValueDataType);

            element = QueryElementService.GetQueryElement<QueryTestModel>(qt => qt.NullMe, QueryOperator.Equals, null);
            Assert.Equal("NullMe", element.Property);
            Assert.Equal(QueryOperator.Equals, element.Operator);
            Assert.Null(element.Value);
            Assert.Equal(ValueDataType.UnsignedInteger, element.ValueDataType);

            element = QueryElementService.GetQueryElement<QueryTestModel>(qt => qt.Offset, QueryOperator.GreaterThanOrEqual, new TimeSpan(0,0,0,0,1));
            Assert.Equal("Offset", element.Property);
            Assert.Equal(QueryOperator.GreaterThanOrEqual, element.Operator);
            Assert.Equal("1", element.Value);
            Assert.Equal(ValueDataType.Double, element.ValueDataType);

            element = QueryElementService.GetQueryElement<QueryTestModel>(qt => qt.SixtyFour, QueryOperator.LessThanOrEqual, 2130);
            Assert.Equal("SixtyFour", element.Property);
            Assert.Equal(QueryOperator.LessThanOrEqual, element.Operator);
            Assert.Equal("2130", element.Value);
            Assert.Equal(ValueDataType.SignedInteger, element.ValueDataType);

            element = QueryElementService.GetQueryElement<QueryTestModel>(qt => qt.Small, QueryOperator.GreaterThan, 2652);
            Assert.Equal("Small", element.Property);
            Assert.Equal(QueryOperator.GreaterThan, element.Operator);
            Assert.Equal("2652", element.Value);
            Assert.Equal(ValueDataType.SignedInteger, element.ValueDataType);

            element = QueryElementService.GetQueryElement<QueryTestModel>(qt => qt.Status, QueryOperator.GreaterThan, 65);
            Assert.Equal("Status", element.Property);
            Assert.Equal(QueryOperator.GreaterThan, element.Operator);
            Assert.Equal("65", element.Value);
            Assert.Equal(ValueDataType.UnsignedInteger, element.ValueDataType);

            element = QueryElementService.GetQueryElement<QueryTestModel>(qt => qt.ThirtyTwo, QueryOperator.LessThan, 22553);
            Assert.Equal("ThirtyTwo", element.Property);
            Assert.Equal(QueryOperator.LessThan, element.Operator);
            Assert.Equal("22553", element.Value);
            Assert.Equal(ValueDataType.SignedInteger, element.ValueDataType);

            element = QueryElementService.GetQueryElement<QueryTestModel>(qt => qt.UArray, QueryOperator.LessThan, 23654789);
            Assert.Equal("UArray", element.Property);
            Assert.Equal(QueryOperator.LessThan, element.Operator);
            Assert.Equal("23654789", element.Value);
            Assert.Equal(ValueDataType.UnsignedInteger, element.ValueDataType);

            element = QueryElementService.GetQueryElement<QueryTestModel>(qt => qt.USixtyFour, QueryOperator.NotEqual, 2365489);
            Assert.Equal("USixtyFour", element.Property);
            Assert.Equal(QueryOperator.NotEqual, element.Operator);
            Assert.Equal("2365489", element.Value);
            Assert.Equal(ValueDataType.UnsignedInteger, element.ValueDataType);

            element = QueryElementService.GetQueryElement<QueryTestModel>(qt => qt.USmall, QueryOperator.Equals, 5214);
            Assert.Equal("USmall", element.Property);
            Assert.Equal(QueryOperator.Equals, element.Operator);
            Assert.Equal("5214", element.Value);
            Assert.Equal(ValueDataType.UnsignedInteger, element.ValueDataType);

            element = QueryElementService.GetQueryElement<QueryTestModel>(qt => qt.UThirtyTwo, QueryOperator.GreaterThanOrEqual, 852147);
            Assert.Equal("UThirtyTwo", element.Property);
            Assert.Equal(QueryOperator.GreaterThanOrEqual, element.Operator);
            Assert.Equal("852147", element.Value);
            Assert.Equal(ValueDataType.UnsignedInteger, element.ValueDataType);

            element = QueryElementService.GetQueryElement<QueryTestModel>(qt => qt.VType, QueryOperator.Equals, Models.Type.NonVolatile);
            Assert.Equal("VType", element.Property);
            Assert.Equal(QueryOperator.Equals, element.Operator);
            Assert.Equal("NonVolatile", element.Value);
            Assert.Equal(ValueDataType.Keyword, element.ValueDataType);
        }

        [Fact]
        public void Can_Proccess_Valid_Nested_Properties()
        {
            var element = QueryElementService.GetQueryElement<QueryTestModel>(qt => qt.Myself.Checked, QueryOperator.Equals, false);
            Assert.Equal("Myself.Checked", element.Property);
            Assert.Equal(QueryOperator.Equals, element.Operator);
            Assert.Equal("0", element.Value);
            Assert.Equal(ValueDataType.UnsignedInteger, element.ValueDataType);

            element = QueryElementService.GetQueryElement<QueryTestModel>(qt => qt.Myself.DateCreated, QueryOperator.GreaterThanOrEqual,
                new DateTime(1970, 1, 1, 0, 0, 0, 60, DateTimeKind.Utc));
            Assert.Equal("Myself.DateCreated", element.Property);
            Assert.Equal(QueryOperator.GreaterThanOrEqual, element.Operator);
            Assert.Equal("60", element.Value);
            Assert.Equal(ValueDataType.Double, element.ValueDataType);

            element = QueryElementService.GetQueryElement<QueryTestModel>(qt => qt.Myself.DateCreatedOffset, QueryOperator.LessThanOrEqual,
                new DateTimeOffset(new DateTime(1970, 1, 1, 0, 0, 1, 0, DateTimeKind.Utc)));
            Assert.Equal("Myself.DateCreatedOffset", element.Property);
            Assert.Equal(QueryOperator.LessThanOrEqual, element.Operator);
            Assert.Equal("1000", element.Value);
            Assert.Equal(ValueDataType.SignedInteger, element.ValueDataType);

            element = QueryElementService.GetQueryElement<QueryTestModel>(qt => qt.Myself.DEnumerable, QueryOperator.GreaterThan, 30.1D);
            Assert.Equal("Myself.DEnumerable", element.Property);
            Assert.Equal(QueryOperator.GreaterThan, element.Operator);
            Assert.Equal(30.1D.ToString(), element.Value);
            Assert.Equal(ValueDataType.Double, element.ValueDataType);

            var guid = Guid.NewGuid();
            element = QueryElementService.GetQueryElement<QueryTestModel>(qt => qt.Myself.Gid, QueryOperator.Equals, guid);
            Assert.Equal("Myself.Gid", element.Property);
            Assert.Equal(QueryOperator.Equals, element.Operator);
            Assert.Equal(guid.ToString(), element.Value);
            Assert.Equal(ValueDataType.Keyword, element.ValueDataType);

            element = QueryElementService.GetQueryElement<QueryTestModel>(qt => qt.Myself.ICanFloat, QueryOperator.LessThan, 2.3F);
            Assert.Equal("Myself.ICanFloat", element.Property);
            Assert.Equal(QueryOperator.LessThan, element.Operator);
            Assert.Equal(2.3F.ToString(), element.Value);
            Assert.Equal(ValueDataType.Double, element.ValueDataType);

            element = QueryElementService.GetQueryElement<QueryTestModel>(qt => qt.Myself.ICanFloatEvenBigger, QueryOperator.NotEqual, 233.22555D);
            Assert.Equal("Myself.ICanFloatEvenBigger", element.Property);
            Assert.Equal(QueryOperator.NotEqual, element.Operator);
            Assert.Equal(233.22555D.ToString().ToString(), element.Value);
            Assert.Equal(ValueDataType.Double, element.ValueDataType);

            element = QueryElementService.GetQueryElement<QueryTestModel>(qt => qt.Myself.Letter, QueryOperator.Equals, 'b');
            Assert.Equal("Myself.Letter", element.Property);
            Assert.Equal(QueryOperator.Equals, element.Operator);
            Assert.Equal('b'.ToString(), element.Value);
            Assert.Equal(ValueDataType.Keyword, element.ValueDataType);

            element = QueryElementService.GetQueryElement<QueryTestModel>(qt => qt.Myself.Name, QueryOperator.Equals, "Final");
            Assert.Equal("Myself.Name", element.Property);
            Assert.Equal(QueryOperator.Equals, element.Operator);
            Assert.Equal("Final", element.Value);
            Assert.Equal(ValueDataType.Keyword, element.ValueDataType);

            element = QueryElementService.GetQueryElement<QueryTestModel>(qt => qt.Myself.NullMe, QueryOperator.Equals, null);
            Assert.Equal("Myself.NullMe", element.Property);
            Assert.Equal(QueryOperator.Equals, element.Operator);
            Assert.Null(element.Value);
            Assert.Equal(ValueDataType.UnsignedInteger, element.ValueDataType);

            element = QueryElementService.GetQueryElement<QueryTestModel>(qt => qt.Myself.Offset, QueryOperator.GreaterThanOrEqual, new TimeSpan(0, 0, 0, 0, 1));
            Assert.Equal("Myself.Offset", element.Property);
            Assert.Equal(QueryOperator.GreaterThanOrEqual, element.Operator);
            Assert.Equal("1", element.Value);
            Assert.Equal(ValueDataType.Double, element.ValueDataType);

            element = QueryElementService.GetQueryElement<QueryTestModel>(qt => qt.Myself.SixtyFour, QueryOperator.LessThanOrEqual, 2130);
            Assert.Equal("Myself.SixtyFour", element.Property);
            Assert.Equal(QueryOperator.LessThanOrEqual, element.Operator);
            Assert.Equal("2130", element.Value);
            Assert.Equal(ValueDataType.SignedInteger, element.ValueDataType);

            element = QueryElementService.GetQueryElement<QueryTestModel>(qt => qt.Myself.Small, QueryOperator.GreaterThan, 2652);
            Assert.Equal("Myself.Small", element.Property);
            Assert.Equal(QueryOperator.GreaterThan, element.Operator);
            Assert.Equal("2652", element.Value);
            Assert.Equal(ValueDataType.SignedInteger, element.ValueDataType);

            element = QueryElementService.GetQueryElement<QueryTestModel>(qt => qt.Myself.Status, QueryOperator.GreaterThan, 65);
            Assert.Equal("Myself.Status", element.Property);
            Assert.Equal(QueryOperator.GreaterThan, element.Operator);
            Assert.Equal("65", element.Value);
            Assert.Equal(ValueDataType.UnsignedInteger, element.ValueDataType);

            element = QueryElementService.GetQueryElement<QueryTestModel>(qt => qt.Myself.ThirtyTwo, QueryOperator.LessThan, 22553);
            Assert.Equal("Myself.ThirtyTwo", element.Property);
            Assert.Equal(QueryOperator.LessThan, element.Operator);
            Assert.Equal("22553", element.Value);
            Assert.Equal(ValueDataType.SignedInteger, element.ValueDataType);

            element = QueryElementService.GetQueryElement<QueryTestModel>(qt => qt.Myself.UArray, QueryOperator.LessThan, 23654789);
            Assert.Equal("Myself.UArray", element.Property);
            Assert.Equal(QueryOperator.LessThan, element.Operator);
            Assert.Equal("23654789", element.Value);
            Assert.Equal(ValueDataType.UnsignedInteger, element.ValueDataType);

            element = QueryElementService.GetQueryElement<QueryTestModel>(qt => qt.Myself.USixtyFour, QueryOperator.NotEqual, 2365489);
            Assert.Equal("Myself.USixtyFour", element.Property);
            Assert.Equal(QueryOperator.NotEqual, element.Operator);
            Assert.Equal("2365489", element.Value);
            Assert.Equal(ValueDataType.UnsignedInteger, element.ValueDataType);

            element = QueryElementService.GetQueryElement<QueryTestModel>(qt => qt.Myself.USmall, QueryOperator.Equals, 5214);
            Assert.Equal("Myself.USmall", element.Property);
            Assert.Equal(QueryOperator.Equals, element.Operator);
            Assert.Equal("5214", element.Value);
            Assert.Equal(ValueDataType.UnsignedInteger, element.ValueDataType);

            element = QueryElementService.GetQueryElement<QueryTestModel>(qt => qt.Myself.UThirtyTwo, QueryOperator.GreaterThanOrEqual, 852147);
            Assert.Equal("Myself.UThirtyTwo", element.Property);
            Assert.Equal(QueryOperator.GreaterThanOrEqual, element.Operator);
            Assert.Equal("852147", element.Value);
            Assert.Equal(ValueDataType.UnsignedInteger, element.ValueDataType);

            element = QueryElementService.GetQueryElement<QueryTestModel>(qt => qt.Myself.VType, QueryOperator.Equals, Models.Type.NonVolatile);
            Assert.Equal("Myself.VType", element.Property);
            Assert.Equal(QueryOperator.Equals, element.Operator);
            Assert.Equal("NonVolatile", element.Value);
            Assert.Equal(ValueDataType.Keyword, element.ValueDataType);
        }

        [Fact]
        public void Throws_Error_On_Invalid_Property()
        {
            Assert.Throws<FinalSymphonyException>(() => QueryElementService.GetQueryElement<QueryTestModel>(qt => qt.Myself, QueryOperator.Equals, Models.Type.NonVolatile));
            Assert.Throws<FormatException>(() => QueryElementService.GetQueryElement<QueryTestModel>(qt => qt.Myself.Checked, QueryOperator.Equals, "abc"));
        }

        [Fact]
        public void Throws_Error_On_Array()
        {
            Assert.Throws<FinalSymphonyException>(() => QueryElementService.GetPropertyDescription<QueryTestModel>(qt => qt.UArray, null, true));
        }
    }
}
