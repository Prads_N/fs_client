﻿namespace FinalSymphonyClient.IntegrationTest
{
    using Models.Attributes;
    using Models;
    using Models.Contracts;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using System.Threading.Tasks;

    class Program
    {
        private static IFinalSymphonyClient _fsClient = new FinalSymphonyClient("fs_test", "4DB6B2B7B4FE178AC7EB08E6A69AC3992ED0E9A73E90A5948C3B2CC02310E327352C5B58E2FA55B2D8FB34BD2DF1B00593654C31F176839C87A396DEC322ACB1");

        public class AggregationTest1
        {
            public int Id { get; set; }
            public string CantAggregateMe { get; set; }
            public IEnumerable<int> CantAggregateMeEither { get; set; }

            public ulong Age { get; set; }
            public double Salary { get; set; }
            public int Views { get; set; }

            public AggregationTest1(int id, ulong age, double salary, int views)
            {
                this.Id = id;
                this.Age = age;
                this.Salary = salary;
                this.Views = views;
            }
        }

        public class AggregateResult
        {
            public int SumId { get; set; }
            public ulong SumAge { get; set; }
            public double SumSalary { get; set; }
            public int SumViews { get; set; }

            public double MeanId { get; set; }
            public double MeanAge { get; set; }
            public double MeanSalary { get; set; }
            public double MeanViews { get; set; }

            public double VarianceId { get; set; }
            public double VarianceAge { get; set; }
            public double VarianceSalary { get; set; }
            public double VarianceViews { get; set; }

            public double StandardDeviationId { get; set; }
            public double StandardDeviationAge { get; set; }
            public double StandardDeviationSalary { get; set; }
            public double StandardDeviationViews { get; set; }

            public int MinId { get; set; }
            public ulong MinAge { get; set; }
            public double MinSalary { get; set; }
            public int MinViews { get; set; }

            public int MaxId { get; set; }
            public ulong MaxAge { get; set; }
            public double MaxSalary { get; set; }
            public int MaxViews { get; set; }
        }

        static async Task Main(string[] args)
        {
            //var i1 = new AggregationTest1(1, 21, 245.6, 997);
            //var i2 = new AggregationTest1(2, 22, 45.67, 1000);
            //var i3 = new AggregationTest1(3, 23, 322.5, 56);
            //var i4 = new AggregationTest1(3, 23, 322.5, 56);
            //var i5 = new AggregationTest1(4, 24, 873.32, 555);
            //var i6 = new AggregationTest1(5, 25, 549.21, 32);
            //var i7 = new AggregationTest1(6, 26, 584.36, 34);
            //var i8 = new AggregationTest1(7, 27, 329.7, 37);
            //var i9 = new AggregationTest1(8, 28, 211.1, 77);
            //var i10 = new AggregationTest1(9, 29, 854.72, 88);

            var stopWatch = new Stopwatch();

            //stopWatch.Start();

            //await _fsClient.Store(i1);
            //await _fsClient.Store(i2);
            //await _fsClient.Store(i3);
            //await _fsClient.Store(i4);
            //await _fsClient.Store(i5);
            //await _fsClient.Store(i6);
            //await _fsClient.Store(i7);
            //await _fsClient.Store(i8);
            //await _fsClient.Store(i9);
            //await _fsClient.Store(i10);

            //stopWatch.Stop();
            //Console.WriteLine($"10 store calls took {stopWatch.ElapsedMilliseconds} ms");

            await _fsClient.Count<MelbourneUvIndex>(qd => qd.Must(p => p.DateTime, QueryOperator.LessThan, new DateTime(2006, 1, 1)));

            stopWatch.Start();

            var count = await _fsClient.Count<MelbourneUvIndex>(qd => qd.Must(p => p.DateTime, QueryOperator.GreaterThan, new DateTime(2006,1,1)));

            stopWatch.Stop();
            Console.WriteLine($"Count took {stopWatch.ElapsedMilliseconds} ms");

            stopWatch.Start();

            var extraction = await _fsClient.Extract<MelbourneUvIndex>(qd => qd.Must(p => p.DateTime, QueryOperator.GreaterThan, new DateTime(2006, 1, 1)));

            stopWatch.Stop();
            Console.WriteLine($"Extract call took {stopWatch.ElapsedMilliseconds} ms");

            stopWatch.Start();

            var aggregation = await _fsClient.Aggregate<MelbourneUvIndex, UvAggregation>(
                qd => qd.Must(p => p.DateTime, QueryOperator.GreaterThan, new DateTime(2006, 1, 1)),
                ad => ad.Sum("SumLatitude", p => p.Latitude)
                        .Sum("SumLongitude", p => p.Longitude)
                        .Sum("SumUvIndex", p => p.UvIndex)
                        .Mean("MeanLatitude", p => p.Latitude)
                        .Mean("MeanLongitude", p => p.Longitude)
                        .Mean("MeanUvIndex", p => p.UvIndex)
                        .Variance("VarianceLatitude", p => p.Latitude)
                        .Variance("VarianceLongitude", p => p.Longitude)
                        .Variance("VarianceUvIndex", p => p.UvIndex)
                        .StandardDeviation("StandardDeviationLatitude", p => p.Latitude)
                        .StandardDeviation("StandardDeviationLongitude", p => p.Longitude)
                        .StandardDeviation("StandardDeviationUvIndex", p => p.UvIndex)
                        .Min("MinLatitude", p => p.Latitude)
                        .Min("MinLongitude", p => p.Longitude)
                        .Min("MinUvIndex", p => p.UvIndex)
                        .Max("MaxLatitude", p => p.Latitude)
                        .Max("MaxLongitude", p => p.Longitude)
                        .Max("MaxUvIndex", p => p.UvIndex));

            stopWatch.Stop();
            Console.WriteLine($"Aggregate took {stopWatch.ElapsedMilliseconds} ms");

            //var aggregationQuery = await _fsClient.Aggregate<AggregationTest1, AggregateResult>(qd => qd.Must(p => p.Id, QueryOperator.GreaterThan, 0),
            //    ad => ad.Sum("SumId", p => p.Id)
            //            .Sum("SumAge", p => p.Age)
            //            .Sum("SumSalary", p => p.Salary)
            //            .Sum("SumViews", p => p.Views)
            //            .Mean("MeanId", p => p.Id)
            //            .Mean("MeanAge", p => p.Age)
            //            .Mean("MeanSalary", p => p.Salary)
            //            .Mean("MeanViews", p => p.Views)
            //            .Variance("VarianceId", p => p.Id)
            //            .Variance("VarianceAge", p => p.Age)
            //            .Variance("VarianceSalary", p => p.Salary)
            //            .Variance("VarianceViews", p => p.Views)
            //            .StandardDeviation("StandardDeviationId", p => p.Id)
            //            .StandardDeviation("StandardDeviationAge", p => p.Age)
            //            .StandardDeviation("StandardDeviationSalary", p => p.Salary)
            //            .StandardDeviation("StandardDeviationViews", p => p.Views)
            //            .Min("MinId", p => p.Id)
            //            .Min("MinAge", p => p.Age)
            //            .Min("MinSalary", p => p.Salary)
            //            .Min("MinViews", p => p.Views)
            //            .Max("MaxId", p => p.Id)
            //            .Max("MaxAge", p => p.Age)
            //            .Max("MaxSalary", p => p.Salary)
            //            .Max("MaxViews", p => p.Views));

            //stopWatch.Stop();
            //Console.WriteLine($"Aggregation took {stopWatch.ElapsedMilliseconds} ms");

            //return;

            //var internationalAirlinesTask = InternationalAirlinesActivity();
            //var optTimeSeriesTask = OptTimeSeries();
            //var sydneyUvIndexTask = PopulateUv<SydneyUvIndex>("uv-sydney-2007.csv");
            //var melbourneUvIndexTask = PopulateUv<MelbourneUvIndex>("uv-melbourne-2007.csv");
            //var animalComplaintTask = PopulateAnimalComplaint();

            //await PopulateUv<MelbourneUvIndex>("uv-melbourne-2007 - Copy.csv");

            //await Task.WhenAll(sydneyUvIndexTask, melbourneUvIndexTask);

            Console.Write("Done...");
            Console.ReadLine();
        }

        static async Task InternationalAirlinesActivity()
        {
            using (var reader = new StreamReader("international_airline_activity_opfltsseats.csv"))
            {
                string line;
                var counter = 1;

                while ((line = reader.ReadLine()) != null)
                {
                    try
                    {
                        var columns = line.Split(",", StringSplitOptions.None);

                        var internationalAirlineActivity = new InternationalAirlineActivity();

                        internationalAirlineActivity.Month = int.Parse(columns[0]);
                        internationalAirlineActivity.In_Out = columns[1];
                        internationalAirlineActivity.AustralianCity = columns[2];
                        internationalAirlineActivity.InternaltionCity = columns[3];
                        internationalAirlineActivity.Airline = columns[4];
                        internationalAirlineActivity.Route = columns[5];
                        internationalAirlineActivity.PortCountry = columns[6];
                        internationalAirlineActivity.PortRegion = columns[7];
                        internationalAirlineActivity.ServiceCountry = columns[8];
                        internationalAirlineActivity.ServiceRegion = columns[9];
                        internationalAirlineActivity.Stops = uint.Parse(columns[10]);
                        internationalAirlineActivity.AllFlights = uint.Parse(columns[11]);
                        internationalAirlineActivity.MaxSeats = uint.Parse(columns[12]);
                        internationalAirlineActivity.Year = uint.Parse(columns[13]);
                        internationalAirlineActivity.MonthNumber = uint.Parse(columns[14]);

                        await _fsClient.Store(internationalAirlineActivity);
                        Console.WriteLine($"InternationalAirlinesActivity: {counter++}");
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("Error occurred: " + ex.Message);
                    }
                }
            }
        }

        static async Task OptTimeSeries()
        {
            using (var reader = new StreamReader("otp_time_series_web.csv"))
            {
                string line;
                var counter = 1;

                while ((line = reader.ReadLine()) != null)
                {
                    try
                    {
                        var columns = line.Split(",", StringSplitOptions.None);

                        var optTs = new OtpTimeSeries();

                        optTs.Route = columns[0];
                        optTs.DepartingPort = columns[1];
                        optTs.ArrivingPort = columns[2];
                        optTs.Airline = columns[3];
                        optTs.Month = int.Parse(columns[4]);
                        optTs.SectorsScheduled = double.Parse(columns[5]);
                        optTs.SectorsFlown = double.Parse(columns[6]);
                        optTs.Cancellation = double.Parse(columns[7]);
                        optTs.DeparturesOnTime = double.Parse(columns[8]);
                        optTs.ArrivalsOnTime = double.Parse(columns[9]);
                        optTs.DeparturesDelayed = double.Parse(columns[10]);
                        optTs.ArrivalsDelayed = double.Parse(columns[11]);
                        optTs.Year = uint.Parse(columns[12]);
                        optTs.MonthNumber = uint.Parse(columns[13]);

                        await _fsClient.Store(optTs);
                        Console.WriteLine($"OptTimeSeries: {counter++}");
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("Error occurred: " + ex.Message);
                    }
                }
            }
        }

        static async Task PopulateUv<T>(string path) where T : BaseUvIndex, new()
        {
            using (var reader = new StreamReader(path))
            {
                var counter = 0;
                var typeName = typeof(T).Name;

                while (true)
                {
                    var lines = new List<string>();
                    string l;

                    while ((l = reader.ReadLine()) != null)
                    {
                        lines.Add(l);

                        if (lines.Count == 10)
                            break;
                    }

                    if (!lines.Any())
                        break;

                    var tasks = lines.Select(async line =>
                    {
                        try
                        {
                            var columns = line.Split(",");

                            var uvIndex = new T();

                            uvIndex.DateTime = DateTime.Parse(columns[0]);
                            uvIndex.Latitude = Double.Parse(columns[1]);
                            uvIndex.Longitude = Double.Parse(columns[2]);
                            uvIndex.UvIndex = Double.Parse(columns[3]);

                            await _fsClient.Store(uvIndex);
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine("Error occurred: " + ex.Message);
                        }
                    });

                    await Task.WhenAll(tasks);

                    counter += 10;
                    Console.WriteLine($"PopulateUv {typeName}: {counter}");
                }
            }
        }

        static async Task PopulateAnimalComplaint()
        {
            using (var reader = new StreamReader("animal-complaints.csv"))
            {
                string line;
                var counter = 1;

                while ((line = reader.ReadLine()) != null)
                {
                    try
                    {
                        var columns = line.Split(",");

                        var animalComplaint = new AnimalComplaint();

                        animalComplaint.AnimalType = columns[0];
                        animalComplaint.ComplaintType = columns[1];

                        var date = columns[2].Split(" ");
                        animalComplaint.Month = date[0];
                        animalComplaint.Year = int.Parse(date[1].Trim());

                        animalComplaint.Suburb = columns[3];
                        animalComplaint.ElectorialDivision = uint.Parse(columns[4].Split(" ")[1].Trim());

                        await _fsClient.Store(animalComplaint);
                        Console.WriteLine($"PopulateAnimalComplaint: {counter++}");
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("Error occurred: " + ex.Message);
                    }
                }
            }
        }

        private class AnimalComplaint
        {
            public string AnimalType { get; set; }
            public string ComplaintType { get; set; }
            public string Month { get; set; }
            public int Year { get; set; }
            public string Suburb { get; set; }
            public uint ElectorialDivision { get; set; }
        }

        private abstract class BaseUvIndex
        {
            public DateTime DateTime { get; set; }
            public double Latitude { get; set; }
            public double Longitude { get; set; }
            public double UvIndex { get; set; }
        }

        private class SydneyUvIndex : BaseUvIndex { }

        private class UvAggregation
        {
            public double SumLatitude { get; set; }
            public double SumLongitude { get; set; }
            public double SumUvIndex { get; set; }
            public double MeanLatitude { get; set; }
            public double MeanLongitude { get; set; }
            public double MeanUvIndex { get; set; }
            public double VarianceLatitude { get; set; }
            public double VarianceLongitude { get; set; }
            public double VarianceUvIndex { get; set; }
            public double StandardDeviationLatitude { get; set; }
            public double StandardDeviationLongitude { get; set; }
            public double StandardDeviationUvIndex { get; set; }
            public double MinLatitude { get; set; }
            public double MinLongitude { get; set; }
            public double MinUvIndex { get; set; }
            public double MaxLatitude { get; set; }
            public double MaxLongitude { get; set; }
            public double MaxUvIndex { get; set; }
        }

        [ContainerName("AggregateMelbourneUvIndex")]
        private class MelbourneUvIndex : BaseUvIndex { }

        private class OtpTimeSeries
        {
            public string Route { get; set; }
            public string DepartingPort { get; set; }
            public string ArrivingPort { get; set; }
            public string Airline { get; set; }
            public int Month { get; set; }
            public double SectorsScheduled { get; set; }
            public double SectorsFlown { get; set; }
            public double Cancellation { get; set; }
            public double DeparturesOnTime { get; set; }
            public double ArrivalsOnTime { get; set; }
            public double DeparturesDelayed { get; set; }
            public double ArrivalsDelayed { get; set; }
            public uint Year { get; set; }
            public uint MonthNumber { get; set; }
        }

        private class InternationalAirlineActivity
        {
            public int Month { get; set; }
            public string In_Out { get; set; }
            public string AustralianCity { get; set; }
            public string InternaltionCity { get; set; }
            public string Airline { get; set; }
            public string Route { get; set; }
            public string PortCountry { get; set; }
            public string PortRegion { get; set; }
            public string ServiceCountry { get; set; }
            public string ServiceRegion { get; set; }
            public uint Stops { get; set; }
            public uint AllFlights { get; set; }
            public uint MaxSeats { get; set; }
            public uint Year { get; set; }
            public uint MonthNumber { get; set; }
        }
    }
}