﻿namespace FinalSymphonyClient
{
    using Exceptions;
    using Extensions;
    using Models.Internal;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;

    internal static class PropertyStorageService
    {
        private static Dictionary<Type, Func<string, PropertyInfo, object, bool, IEnumerable<PropertyStorage>>> _propertyToPropertyStorage;

        static PropertyStorageService()
        {
            PopulatePropertyStorageMethods();
        }

        public static IEnumerable<PropertyStorage> GetPropertyData(string propertyPrefix, IEnumerable<PropertyInfo> properties, object instance)
        {
            var result = new List<PropertyStorage>();

            foreach (var property in properties)
                result.AddRange(GetPropertyStorage(propertyPrefix, property, instance));

            return result;
        }

        private static IEnumerable<PropertyStorage> GetPropertyStorage(string propertyPrefix, PropertyInfo property, object instance)
        {
            // Primitives
            if (TypeService.IsSimpleType(property.PropertyType))
            {
                if (!_propertyToPropertyStorage.TryGetValue(property.PropertyType, out var func))
                {
                    if (!property.PropertyType.IsEnum)
                        throw new FinalSymphonyException($"Unknown property type {property.PropertyType.Name}");

                    func = _propertyToPropertyStorage[typeof(Enum)];
                }

                return func(propertyPrefix, property, instance, false);
            }

            // Array
            if (property.PropertyType.IsArray)
            {
                var collectionType = property.PropertyType.GetElementType();

                if (!TypeService.IsSimpleType(collectionType))
                    throw new FinalSymphonyException("Array of nested object is not supported");

                if (!_propertyToPropertyStorage.TryGetValue(collectionType, out var func))
                    throw new FinalSymphonyException($"Unknown property type {property.PropertyType.Name}");

                return GetArrayPropertyStorage(propertyPrefix, property, property.GetValue(instance), func);
            }

            // IEnumerable
            if (property.PropertyType.GetInterfaces().Contains(typeof(IEnumerable)))
                return _propertyToPropertyStorage[typeof(IEnumerable<>)](propertyPrefix, property, instance, true);

            //Nullable
            if (property.PropertyType.IsGenericType && property.PropertyType.GetGenericTypeDefinition() == typeof(Nullable<>))
                return GetNullablePropertyStorage(propertyPrefix, property, instance);

            // Nested Object
            return GetPropertyData(GetPropertyName(propertyPrefix, property.Name), TypeService.GetProperties(property.PropertyType), property.GetValue(instance));
        }

        private static IEnumerable<PropertyStorage> GetNullablePropertyStorage(string propertyPrefix, PropertyInfo property, object instance)
        {
            var type = Nullable.GetUnderlyingType(property.PropertyType);

            if (!TypeService.IsSimpleType(type))
                throw new FinalSymphonyException("Collection of nested object is not supported");

            if (!_propertyToPropertyStorage.TryGetValue(type, out var func))
                throw new FinalSymphonyException($"Unknown property type {property.PropertyType.Name}");

            var value = property.GetValue(instance);
            return func(propertyPrefix, property, value, true);
        }

        private static void PopulatePropertyStorageMethods()
        {
            _propertyToPropertyStorage = new Dictionary<Type, Func<string, PropertyInfo, object, bool, IEnumerable<PropertyStorage>>>();

            Func<string, PropertyInfo, object, bool, IEnumerable<PropertyStorage>> unsignedIntegers = (propertyPrefix, propertyInfo, instance, instanceIsValue) =>
                new[] { GetPropertyStorage(propertyPrefix, propertyInfo, instance, ValueDataType.UnsignedInteger, instanceIsValue) };

            Func<string, PropertyInfo, object, bool, IEnumerable<PropertyStorage>> signedIntegers = (propertyPrefix, propertyInfo, instance, instanceIsValue) =>
                new[] { GetPropertyStorage(propertyPrefix, propertyInfo, instance, ValueDataType.SignedInteger, instanceIsValue) };

            Func<string, PropertyInfo, object, bool, IEnumerable<PropertyStorage>> keywords = (propertyPrefix, propertyInfo, instance, instanceIsValue) =>
                new[] { GetPropertyStorage(propertyPrefix, propertyInfo, instance, ValueDataType.Keyword, instanceIsValue) };

            Func<string, PropertyInfo, object, bool, IEnumerable<PropertyStorage>> doubles = (propertyPrefix, propertyInfo, instance, instanceIsValue) =>
                new[] { GetPropertyStorage(propertyPrefix, propertyInfo, instance, ValueDataType.Double, instanceIsValue) };

            Func<string, PropertyInfo, object, bool, IEnumerable<PropertyStorage>> dateTime = (propertyPrefix, propertyInfo, instance, instanceIsValue) =>
                new[] { GetDateTimePropertyStorage(propertyPrefix, propertyInfo, instance, instanceIsValue) };

            Func<string, PropertyInfo, object, bool, IEnumerable<PropertyStorage>> dateTimeOffset = (propertyPrefix, propertyInfo, instance, instanceIsValue) =>
                new[] { GetDateTimeOffsetPropertyStorage(propertyPrefix, propertyInfo, instance, instanceIsValue) };

            Func<string, PropertyInfo, object, bool, IEnumerable<PropertyStorage>> timespan = (propertyPrefix, propertyInfo, instance, instanceIsValue) =>
                new[] { GetDateTimespanPropertyStorage(propertyPrefix, propertyInfo, instance, instanceIsValue) };

            Func<string, PropertyInfo, object, bool, IEnumerable<PropertyStorage>> iEnumerable = (propertyPrefix, propertyInfo, instance, instanceIsValue) =>
                GetIEnumerablePropertyStorage(propertyPrefix, propertyInfo, instance);

            _propertyToPropertyStorage.Add(typeof(bool), unsignedIntegers);
            _propertyToPropertyStorage.Add(typeof(byte), unsignedIntegers);
            _propertyToPropertyStorage.Add(typeof(ushort), unsignedIntegers);
            _propertyToPropertyStorage.Add(typeof(uint), unsignedIntegers);
            _propertyToPropertyStorage.Add(typeof(ulong), unsignedIntegers);

            _propertyToPropertyStorage.Add(typeof(short), signedIntegers);
            _propertyToPropertyStorage.Add(typeof(int), signedIntegers);
            _propertyToPropertyStorage.Add(typeof(long), signedIntegers);

            _propertyToPropertyStorage.Add(typeof(float), doubles);
            _propertyToPropertyStorage.Add(typeof(double), doubles);
            _propertyToPropertyStorage.Add(typeof(decimal), doubles);

            _propertyToPropertyStorage.Add(typeof(char), keywords);
            _propertyToPropertyStorage.Add(typeof(string), keywords);
            _propertyToPropertyStorage.Add(typeof(Enum), keywords);
            _propertyToPropertyStorage.Add(typeof(Guid), keywords);

            _propertyToPropertyStorage.Add(typeof(DateTime), dateTime);
            _propertyToPropertyStorage.Add(typeof(DateTimeOffset), dateTimeOffset);
            _propertyToPropertyStorage.Add(typeof(TimeSpan), timespan);

            _propertyToPropertyStorage.Add(typeof(IEnumerable<>), iEnumerable);
        }

        private static PropertyStorage GetPropertyStorage(string propertyPrefix, PropertyInfo propertyInfo, object instance, ValueDataType valueDataType, bool instanceIsValue)
        {
            object value;

            if (valueDataType == ValueDataType.Keyword)
                value = instanceIsValue ? instance?.ToString() : propertyInfo.GetValue(instance)?.ToString();
            else
                value = instanceIsValue ? instance : propertyInfo.GetValue(instance);

            return new PropertyStorage
            {
                PropertyName = GetPropertyName(propertyPrefix, propertyInfo.Name),
                Value = value,
                ValueDataType = valueDataType
            };
        }

        private static PropertyStorage GetDateTimePropertyStorage(string propertyPrefix, PropertyInfo propertyInfo, object instance, bool instanceIsValue)
        {
            var value = (instanceIsValue ? (DateTime)instance : (DateTime)propertyInfo.GetValue(instance)).ToTimestampMilliseconds();

            return new PropertyStorage
            {
                PropertyName = GetPropertyName(propertyPrefix, propertyInfo.Name),
                Value = value,
                ValueDataType = ValueDataType.Double
            };
        }

        private static PropertyStorage GetDateTimeOffsetPropertyStorage(string propertyPrefix, PropertyInfo propertyInfo, object instance, bool instanceIsValue)
        {
            var value = (instanceIsValue ? (DateTimeOffset)instance : (DateTimeOffset)propertyInfo.GetValue(instance)).ToUniversalTime()
                .ToUnixTimeMilliseconds();

            return new PropertyStorage
            {
                PropertyName = GetPropertyName(propertyPrefix, propertyInfo.Name),
                Value = value,
                ValueDataType = ValueDataType.SignedInteger
            };
        }

        private static PropertyStorage GetDateTimespanPropertyStorage(string propertyPrefix, PropertyInfo propertyInfo, object instance, bool instanceIsValue)
        {
            var value = (instanceIsValue ? (TimeSpan)instance : (TimeSpan)propertyInfo.GetValue(instance)).TotalMilliseconds;

            return new PropertyStorage
            {
                PropertyName = GetPropertyName(propertyPrefix, propertyInfo.Name),
                Value = value,
                ValueDataType = ValueDataType.Double
            };
        }

        private static IEnumerable<PropertyStorage> GetIEnumerablePropertyStorage(string propertyPrefix, PropertyInfo propertyInfo, object instance)
        {
            var collection = propertyInfo.GetValue(instance);
            var collectionType = propertyInfo.PropertyType.GetGenericArguments()[0];

            if (!TypeService.IsSimpleType(collectionType))
                throw new FinalSymphonyException("Collection of nested object is not supported");

            if (!_propertyToPropertyStorage.TryGetValue(collectionType, out var func))
                throw new FinalSymphonyException($"Unknown property type {propertyInfo.PropertyType.Name}");

            return GetArrayPropertyStorage(propertyPrefix, propertyInfo, collection, func);
        }

        private static IEnumerable<PropertyStorage> GetArrayPropertyStorage(string propertyPrefix, PropertyInfo propertyInfo, object collection,
            Func<string, PropertyInfo, object, bool, IEnumerable<PropertyStorage>> func)
        {
            //Create a distinct list of values
            var hashSet = new HashSet<object>();

            if (collection == null)
                return Enumerable.Empty<PropertyStorage>();

            foreach (var value in ((IEnumerable)collection))
                hashSet.Add(value);

            var result = new List<PropertyStorage>();

            foreach (var value in hashSet)
                result.AddRange(func(propertyPrefix, propertyInfo, value, true));

            return result;
        }

        private static string GetPropertyName(string propertyPrefix, string propertyName)
        {
            if (String.IsNullOrWhiteSpace(propertyPrefix))
                return propertyName;

            return $"{propertyPrefix}.{propertyName}";
        }
    }
}
