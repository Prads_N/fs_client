﻿namespace FinalSymphonyClient
{
    using global::FinalSymphonyClient.Models.Attributes;
    using System;
    using System.Collections.Concurrent;
    using System.Collections.Generic;
    using System.Reflection;

    internal static class TypeService
    {
        private static readonly ConcurrentDictionary<Type, CachedValidation> _validationCache;
        private static readonly ConcurrentDictionary<Type, IEnumerable<PropertyInfo>> _cachedProperties;
        private static readonly ConcurrentDictionary<Type, string> _cachedContainerName;

        private static readonly HashSet<Type> _simpleTypes;

        static TypeService()
        {
            _validationCache = new ConcurrentDictionary<Type, CachedValidation>();
            _cachedProperties = new ConcurrentDictionary<Type, IEnumerable<PropertyInfo>>();
            _cachedContainerName = new ConcurrentDictionary<Type, string>();

            _simpleTypes = new HashSet<Type>(new Type[] {
                                typeof(Enum),
                                typeof(String),
                                typeof(Decimal),
                                typeof(DateTime),
                                typeof(DateTimeOffset),
                                typeof(TimeSpan),
                                typeof(Guid) });
        }

        public static CachedValidation GetValidation<TObject>()
        {
            if (_validationCache.TryGetValue(typeof(TObject), out var cachedData))
                return cachedData;

            var properties = typeof(TObject).GetProperties();
            var invalidReason = "";

            var isValid = ValidateType("", properties, ref invalidReason);

            var cachedValidation = new CachedValidation(isValid, invalidReason);
            _validationCache.TryAdd(typeof(TObject), cachedValidation);

            return cachedValidation;
        }

        public static string GetContainerName<TObject>()
        {
            if (_cachedContainerName.TryGetValue(typeof(TObject), out var containerName))
                return containerName;

            var containerNameAttribute = (ContainerNameAttribute)Attribute.GetCustomAttribute(typeof(TObject), typeof(ContainerNameAttribute));
            containerName = containerNameAttribute == null ? typeof(TObject).Name : containerNameAttribute.Name;

            _cachedContainerName.TryAdd(typeof(TObject), containerName);
            return containerName;
        }

        public static IEnumerable<PropertyInfo> GetProperties<TObject>()
        {
            return GetProperties(typeof(TObject));
        }

        public static IEnumerable<PropertyInfo> GetProperties(Type type)
        {
            if (_cachedProperties.TryGetValue(type, out var properties))
                return properties;

            properties = type.GetProperties();
            _cachedProperties.TryAdd(type, properties);

            return properties;
        }

        public static bool IsSimpleType(Type type)
        {
            return type.IsPrimitive ||
                type.IsEnum ||
                _simpleTypes.Contains(type) ||
                Convert.GetTypeCode(type) != TypeCode.Object;
        }

        private static bool ValidateType(string propertyPrefix, IEnumerable<PropertyInfo> properties, ref string invalidReason)
        {
            const int maxPropertyNameLength = 255;

            foreach (var property in properties)
            {
                var propertyName = $"{propertyPrefix}.{property.Name}";

                if (propertyName.Length > maxPropertyNameLength)
                {
                    invalidReason = "Property name is too long";
                    return false;
                }

                if (IsSimpleType(property.PropertyType))
                    continue;

                if (property.PropertyType.IsAssignableFrom(typeof(IEnumerable<>)))
                {
                    if (!IsSimpleType(property.PropertyType.GetGenericArguments()[0]))
                    {
                        invalidReason = "Array of object is not supported";
                        return false;
                    }
                }

                if (!ValidateType(propertyName, property.PropertyType.GetProperties(), ref invalidReason))
                    return false;
            }

            return true;
        }
    }

    internal class CachedValidation
    {
        public bool IsValid { get; }
        public string InvalidReason { get; set; }

        public CachedValidation(bool isValid, string invalidReason)
        {
            this.IsValid = isValid;
            this.InvalidReason = invalidReason;
        }
    }
}