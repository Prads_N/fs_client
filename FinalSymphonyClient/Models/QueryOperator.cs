﻿namespace FinalSymphonyClient.Models
{
    public enum QueryOperator
    {
        Equals = 1,
        NotEqual = 2,
        GreaterThanOrEqual = 3,
        LessThanOrEqual = 4,
        GreaterThan = 5,
        LessThan = 6
    }
}