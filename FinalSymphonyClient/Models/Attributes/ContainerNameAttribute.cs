﻿namespace FinalSymphonyClient.Models.Attributes
{
    using System;

    public class ContainerNameAttribute : Attribute
    {
        public string Name { get; }

        public ContainerNameAttribute(string name)
        {
            Name = name;
        }
    }
}