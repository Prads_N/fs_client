﻿namespace FinalSymphonyClient.Models
{
    using global::FinalSymphonyClient.Exceptions;
    using global::FinalSymphonyClient.Models.Internal;
    using System;
    using System.Collections.Generic;
    using System.Linq.Expressions;

    public class AggregationDescriptor<TEvent>
    {
        private readonly List<AggregationElement> _elements;

        public AggregationDescriptor()
        {
            _elements = new List<AggregationElement>();
        }

        public AggregationDescriptor<TEvent> Sum(string propertyAlias, Expression<Func<TEvent, object>> property)
        {
            return AggregateInternal(propertyAlias, property, AggregationType.Sum);
        }

        public AggregationDescriptor<TEvent> Mean(string propertyAlias, Expression<Func<TEvent, object>> property)
        {
            return AggregateInternal(propertyAlias, property, AggregationType.Mean);
        }

        public AggregationDescriptor<TEvent> Variance(string propertyAlias, Expression<Func<TEvent, object>> property)
        {
            return AggregateInternal(propertyAlias, property, AggregationType.Variance);
        }

        public AggregationDescriptor<TEvent> StandardDeviation(string propertyAlias, Expression<Func<TEvent, object>> property)
        {
            return AggregateInternal(propertyAlias, property, AggregationType.StandardDeviation);
        }

        public AggregationDescriptor<TEvent> Min(string propertyAlias, Expression<Func<TEvent, object>> property)
        {
            return AggregateInternal(propertyAlias, property, AggregationType.Min);
        }

        public AggregationDescriptor<TEvent> Max(string propertyAlias, Expression<Func<TEvent, object>> property)
        {
            return AggregateInternal(propertyAlias, property, AggregationType.Max);
        }

        internal AggregationRequest GetAggregationRequest()
        {
            return new AggregationRequest
            {
                AggregationElements = _elements
            };
        }

        private (string Path, ValueDataType DataType) GetPropertyDescription(Expression<Func<TEvent, object>> property)
        {
            var propertyDescription = QueryElementService.GetPropertyDescription(property, null, true);

            var dataType = propertyDescription.DataType;
            var path = propertyDescription.Path;

            if (dataType == ValueDataType.Keyword)
                throw new FinalSymphonyException("Cannot aggregate on string types");

            return (path, dataType);
        }

        private AggregationDescriptor<TEvent> AggregateInternal(string propertyAlias, Expression<Func<TEvent, object>> property, AggregationType aggregationType)
        {
            if (String.IsNullOrWhiteSpace(propertyAlias))
                throw new FinalSymphonyException("Property alias cannot be null or empty");

            var propertyDescription = this.GetPropertyDescription(property);

            _elements.Add(new AggregationElement
            {
                AggregationType = aggregationType,
                Alias = propertyAlias,
                Property = propertyDescription.Path,
                ValueDataType = propertyDescription.DataType
            });

            return this;
        }
    }
}