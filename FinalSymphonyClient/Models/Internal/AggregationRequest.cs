﻿namespace FinalSymphonyClient.Models.Internal
{
    using System.Collections.Generic;

    internal class AggregationRequest 
    {
        public IEnumerable<AggregationElement> AggregationElements { get; set; }
    }
}