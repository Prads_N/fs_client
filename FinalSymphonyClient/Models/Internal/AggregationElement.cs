﻿namespace FinalSymphonyClient.Models.Internal
{
    internal class AggregationElement
    {
        public string Alias { get; set; }
        public string Property { get; set; }
        public ValueDataType ValueDataType { get; set; }
        public AggregationType AggregationType { get; set; }
    }
}