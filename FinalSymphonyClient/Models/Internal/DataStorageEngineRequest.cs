﻿namespace FinalSymphonyClient.Models.Internal
{
    using System.Collections.Generic;

    internal class DataStorageEngineRequest
    {
        public IEnumerable<PropertyStorage> Properties { get; set; }
        public string ContainerName { get; set; }
    }
}