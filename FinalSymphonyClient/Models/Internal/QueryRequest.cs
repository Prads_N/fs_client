﻿namespace FinalSymphonyClient.Models.Internal
{
    internal class QueryRequest
    {
        public string ContainerName { get; set; }
        public ReturnType ReturnType { get; set; }
        public QueryType QueryType { get; set; }

        public SimpleQueryEngineModel QueryData { get; set; }
        public AggregationRequest AggregationRequest { get; set; }
    }
}
