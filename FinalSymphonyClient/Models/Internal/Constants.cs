﻿namespace FinalSymphonyClient.Models.Internal
{
    internal static class Constants
    {
        public static class Auth
        {
            public static class Scopes
            {
                public const string Client = "fs_client";
            }
        }
    }
}