﻿namespace FinalSymphonyClient.Models.Internal
{
    using System.Collections.Generic;

    internal class SimpleQueryEngineModel
    {
        public IEnumerable<QueryElement> Must { get; set; }
        public IEnumerable<QueryElement> Should { get; set; }
    }
}
