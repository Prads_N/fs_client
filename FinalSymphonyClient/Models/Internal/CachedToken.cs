﻿namespace FinalSymphonyClient.Models.Internal
{
    using IdentityModel.Client;
    using System;

    internal class CachedToken
    {
        public string AccessToken { get; }
        public DateTime Expiry { get; }

        public bool HasExpired => Expiry <= DateTime.UtcNow;

        public CachedToken(TokenResponse token)
        {
            this.AccessToken = token.AccessToken;
            this.Expiry = DateTime.UtcNow.AddSeconds(token.ExpiresIn - 60);
        }
    }
}