﻿namespace FinalSymphonyClient.Models.Internal
{
    internal class SimpleQueryRequest
    {
        public string ContainerName { get; set; }
        public ReturnType ReturnType { get; set; }
        public QueryType QueryType { get; set; }

        public SimpleQueryEngineModel QueryData { get; set; }
    }
}
