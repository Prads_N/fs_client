﻿namespace FinalSymphonyClient.Models.Internal.Contracts
{
    using System.Threading.Tasks;

    internal interface IFinalSymphonyApiClient
    {
        Task Store(DataStorageRequest request);
        Task<TOutput> Query<TOutput>(QueryRequest request);
    }
}