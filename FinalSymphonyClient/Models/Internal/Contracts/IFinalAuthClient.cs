﻿namespace FinalSymphonyClient.Models.Internal.Contracts
{
    using IdentityModel.Client;
    using System.Threading.Tasks;

    internal interface IFinalAuthClient
    {
        Task<TokenResponse> GetToken();
    }
}