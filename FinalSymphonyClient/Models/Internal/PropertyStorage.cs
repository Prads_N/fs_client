﻿namespace FinalSymphonyClient.Models.Internal
{
    internal class PropertyStorage
    {
        public string PropertyName { get; set; }
        public ValueDataType ValueDataType { get; set; }
        public object Value { get; set; }
    }
}