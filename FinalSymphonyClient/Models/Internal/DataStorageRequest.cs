﻿namespace FinalSymphonyClient.Models.Internal
{
    internal class DataStorageRequest
    {
        public DataStorageEngineRequest EngineRequest { get; set; }
        public string DataJson { get; set; }
    }
}