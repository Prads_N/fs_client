﻿namespace FinalSymphonyClient.Models.Internal
{
    internal class QueryElement
    {
        public string Property { get; set; }
        public QueryOperator Operator { get; set; }
        public string Value { get; set; }
        public ValueDataType ValueDataType { get; set; }
    }
}