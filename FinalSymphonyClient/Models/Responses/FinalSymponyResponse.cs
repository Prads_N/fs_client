﻿namespace FinalSymphonyClient.Models.Responses
{
    public class FinalSymponyResponse<TResult>
    {
        public TResult Data { get; set; }
    }
}