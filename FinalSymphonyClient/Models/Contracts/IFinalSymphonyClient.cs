﻿namespace FinalSymphonyClient.Models.Contracts
{
    using global::FinalSymphonyClient.Models.Responses;
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    public interface IFinalSymphonyClient
    {
        Task Store<TObject>(TObject value);
        Task<CountResponse> Count<TEvent>(Action<SimpleQueryDescriptor<TEvent>> queryDescriptor);
        Task<ExtractionResponse<IEnumerable<TEvent>>> Extract<TEvent>(Action<SimpleQueryDescriptor<TEvent>> queryDescriptor);
        Task<AggregationResponse<TOutput>> Aggregate<TEvent, TOutput>(Action<SimpleQueryDescriptor<TEvent>> queryDescriptor, Action<AggregationDescriptor<TEvent>> aggregationDescriptor);
    }
}