﻿namespace FinalSymphonyClient.Models
{
    public enum ReturnType
    {
        Count = 1,
        Extraction = 2,
        Aggregation = 3
    }
}