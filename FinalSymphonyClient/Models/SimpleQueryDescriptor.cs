﻿namespace FinalSymphonyClient.Models
{
    using Exceptions;
    using Models.Internal;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;

    public class SimpleQueryDescriptor<TEvent>
    {
        private readonly List<QueryElement> _must;
        private readonly List<QueryElement> _should;

        public SimpleQueryDescriptor() 
        {
            _must = new List<QueryElement>();
            _should = new List<QueryElement>();
        }

        public SimpleQueryDescriptor<TEvent> Must(Expression<Func<TEvent, object>> property, QueryOperator queryOperator, object value)
        {
            _must.Add(QueryElementService.GetQueryElement(property, queryOperator, value));
            return this;
        }

        public SimpleQueryDescriptor<TEvent> Should(Expression<Func<TEvent, object>> property, QueryOperator queryOperator, object value)
        {
            _should.Add(QueryElementService.GetQueryElement(property, queryOperator, value));
            return this;
        }

        internal QueryRequest GetRequest(ReturnType returnType)
        {
            if (!_must.Any() && !_should.Any())
                throw new FinalSymphonyException("Query is empty");

            return new QueryRequest
            {
                ContainerName = TypeService.GetContainerName<TEvent>(),
                QueryType = QueryType.SIMPLE,
                ReturnType = returnType,
                QueryData = new SimpleQueryEngineModel
                {
                    Must = _must,
                    Should = _should
                }
            };
        }
    }
}