﻿namespace FinalSymphonyClient.ApiClients
{
    using Models.Internal;
    using Models.Internal.Contracts;
    using Primitives;
    using RestSharp;
    using System.Threading.Tasks;

    internal class FinalSymphonyApiClient : BaseAuthenticatedApiClient, IFinalSymphonyApiClient
    {
        public FinalSymphonyApiClient(string endpoint, IFinalAuthClient finalAuthClient) 
            : base(endpoint, finalAuthClient) { }

        public Task Store(DataStorageRequest request)
        {
            var restRequest = new RestRequest("v1/documents", Method.POST);
            restRequest.AddJsonBody(request);

            return Execute(restRequest);
        }

        public Task<TOutput> Query<TOutput>(QueryRequest request)
        {
            var restRequest = new RestRequest("v1/query", Method.POST);
            restRequest.AddJsonBody(request);

            return Execute<TOutput>(restRequest);
        }
    }
}