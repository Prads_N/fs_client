﻿namespace FinalSymphonyClient.Extensions
{
    using System;

    internal static class DateTimeExtensions
    {
        public static double ToTimestampMilliseconds(this DateTime dateTime)
        {
            var timeSpan = dateTime.ToUniversalTime() - new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            return timeSpan.TotalMilliseconds;
        }
    }
}