﻿namespace FinalSymphonyClient.Extensions
{
    using Newtonsoft.Json;

    internal static class StringExtensions
    {
        public static T JsonToObject<T>(this string value)
        {
            return JsonConvert.DeserializeObject<T>(value);
        }
    }
}