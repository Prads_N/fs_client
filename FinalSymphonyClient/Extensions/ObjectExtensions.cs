﻿namespace FinalSymphonyClient.Extensions
{
    using Newtonsoft.Json;

    internal static class ObjectExtensions
    {
        public static string ToJson(this object value)
        {
            return JsonConvert.SerializeObject(value);
        }
    }
}