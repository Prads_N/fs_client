﻿using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("FinalSymphonyClient.Tests")]
namespace FinalSymphonyClient
{
    using Exceptions;
    using Extensions;
    using Models.Internal.Contracts;
    using Models.Internal;
    using System.Threading.Tasks;
    using ApiClients;
    using Models.Contracts;
    using global::FinalSymphonyClient.Models.Responses;
    using global::FinalSymphonyClient.Models;
    using System;
    using System.Collections.Generic;

    public class FinalSymphonyClient : IFinalSymphonyClient
    {
        private readonly IFinalSymphonyApiClient _finalSymphonyApiClient;

        public FinalSymphonyClient(string clientId, string clientSecret)
        {
            _finalSymphonyApiClient = new FinalSymphonyApiClient("http://localhost:5010", 
                new FinalAuthClient("https://localhost:5001", clientId, clientSecret));
        }

        public Task Store<TObject>(TObject value)
        {
            var validation = TypeService.GetValidation<TObject>();

            if (!validation.IsValid)
                throw new FinalSymphonyException(validation.InvalidReason);

            var containerName = TypeService.GetContainerName<TObject>();

            var properties = TypeService.GetProperties<TObject>();
            var propertyData = PropertyStorageService.GetPropertyData("", properties, value);

            var request = new DataStorageRequest
            {
                DataJson = value.ToJson(),
                EngineRequest = new DataStorageEngineRequest
                {
                    ContainerName = containerName,
                    Properties = propertyData
                }
            };

            return _finalSymphonyApiClient.Store(request);
        }

        public Task<CountResponse> Count<TEvent>(Action<SimpleQueryDescriptor<TEvent>> queryDescriptor)
        {
            return _finalSymphonyApiClient.Query<CountResponse>(this.GetQuery(queryDescriptor).GetRequest(ReturnType.Count));
        }

        public Task<ExtractionResponse<IEnumerable<TEvent>>> Extract<TEvent>(Action<SimpleQueryDescriptor<TEvent>> queryDescriptor)
        {
            return _finalSymphonyApiClient.Query<ExtractionResponse<IEnumerable<TEvent>>>(this.GetQuery(queryDescriptor)
                .GetRequest(ReturnType.Extraction));
        }

        public Task<AggregationResponse<TOutput>> Aggregate<TEvent, TOutput>(Action<SimpleQueryDescriptor<TEvent>> queryDescriptor, 
            Action<AggregationDescriptor<TEvent>> aggregationDescriptor)
        {
            var request = this.GetQuery(queryDescriptor).GetRequest(ReturnType.Aggregation);

            var aggregation = new AggregationDescriptor<TEvent>();
            aggregationDescriptor(aggregation);

            request.AggregationRequest = aggregation.GetAggregationRequest();
            return _finalSymphonyApiClient.Query<AggregationResponse<TOutput>>(request);
        }

        private SimpleQueryDescriptor<TEvent> GetQuery<TEvent>(Action<SimpleQueryDescriptor<TEvent>> func)
        {
            var query = new SimpleQueryDescriptor<TEvent>();
            func(query);

            return query;
        }
    }
}