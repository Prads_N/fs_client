﻿namespace FinalSymphonyClient.Exceptions
{
    using System;

    public class FinalSymphonyException : Exception
    {
        public FinalSymphonyException(string message) : base(message) { }
        public FinalSymphonyException(string message, Exception innerException) : base(message, innerException) { }
    }
}