﻿namespace FinalSymphonyClient
{
    using Exceptions;
    using global::FinalSymphonyClient.Extensions;
    using Models;
    using Models.Internal;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;

    internal static class QueryElementService
    {
        private static Dictionary<Type, (ValueDataType, Func<object, object>)> _valueFunctions;

        static QueryElementService()
        {
            PopulateValueFunctions();
        }

        public static QueryElement GetQueryElement<TEvent>(Expression<Func<TEvent, object>> property, QueryOperator queryOperator, object value)
        {
            var propertyDescription = GetPropertyDescription(property, value);

            return new QueryElement
            {
                Operator = queryOperator,
                Property = propertyDescription.Path,
                Value = propertyDescription.Value?.ToString(),
                ValueDataType = propertyDescription.DataType
            };
        }

        public static (string Path, ValueDataType DataType, object Value) GetPropertyDescription<TEvent>(Expression<Func<TEvent, object>> expression, 
            object value, bool throwIfArray = false)
        {
            if (expression.Body is UnaryExpression)
            {
                var unaryExpression = expression.Body as UnaryExpression;
                var valueInformation = GetValueInformation(unaryExpression.Operand.Type, value, throwIfArray);

                return (ToPropertyPath(unaryExpression.Operand.ToString()), valueInformation.ValueDataType, valueInformation.Value);
            }
            else
            {
                var valueInformation = GetValueInformation(expression.Body.Type, value, throwIfArray);
                return (ToPropertyPath(expression.Body.ToString()), valueInformation.ValueDataType, valueInformation.Value);
            }
        }

        private static string ToPropertyPath(string path)
        {
            return String.Join('.', path.Split(".").Skip(1));
        }

        private static (ValueDataType ValueDataType, object Value) GetValueInformation(Type type, object value, bool throwIfArray)
        {
            // Array
            if (type.IsArray)
            {
                if (throwIfArray)
                    throw new FinalSymphonyException("Array is not supported for this operation");

                var elementType = type.GetElementType();
                return GetValueInformation(elementType, value, false);
            }

            // IEnumerable
            if (type != typeof(string) && type.GetInterfaces().Contains(typeof(IEnumerable)))
            {
                if (throwIfArray)
                    throw new FinalSymphonyException("Array is not supported for this operation");

                var collectionType = type.GetGenericArguments()[0];
                return GetValueInformation(collectionType, value, false);
            }

            //Nullable
            if (type.IsGenericType && type.GetGenericTypeDefinition() == typeof(Nullable<>))
            {
                var underlyingType = Nullable.GetUnderlyingType(type);
                return GetValueInformation(underlyingType, value, throwIfArray);
            }

            if (!_valueFunctions.TryGetValue(type, out var function))
            {
                if (type.IsEnum)
                    function = _valueFunctions[typeof(Enum)];
                else
                    throw new FinalSymphonyException($"Type {type} is not supported");
            }

            var newValue = value == null ? null : function.Item2(value);
            ValidateValue(function.Item1, newValue, type);

            return (function.Item1, newValue);
        }

        private static void ValidateValue(ValueDataType valueDataType, object value, Type originalType)
        {
            if (value == null)
                return;

            switch (valueDataType)
            {
                case ValueDataType.Double:
                    if (!Double.TryParse(value.ToString(), out _))
                        throw new FinalSymphonyException($"Value is not of type {originalType.Name}");

                    break;
                case ValueDataType.SignedInteger:
                    if (!Int64.TryParse(value.ToString(), out _))
                        throw new FinalSymphonyException($"Value is not of type {originalType.Name}");

                    break;
                case ValueDataType.UnsignedInteger:
                    if (!UInt64.TryParse(value.ToString(), out _))
                        throw new FinalSymphonyException($"Value is not of type {originalType.Name}");

                    break;
            }
        }

        private static void PopulateValueFunctions()
        {
            _valueFunctions = new Dictionary<Type, (ValueDataType, Func<object, object>)>();

            _valueFunctions.Add(typeof(bool), (ValueDataType.UnsignedInteger, value => Convert.ToUInt64(value).ToString()));
            _valueFunctions.Add(typeof(byte), (ValueDataType.UnsignedInteger, value => value.ToString()));
            _valueFunctions.Add(typeof(ushort), (ValueDataType.UnsignedInteger, value => value.ToString()));
            _valueFunctions.Add(typeof(uint), (ValueDataType.UnsignedInteger, value => value.ToString()));
            _valueFunctions.Add(typeof(ulong), (ValueDataType.UnsignedInteger, value => value.ToString()));

            _valueFunctions.Add(typeof(short), (ValueDataType.SignedInteger, value => value.ToString()));
            _valueFunctions.Add(typeof(int), (ValueDataType.SignedInteger, value => value.ToString()));
            _valueFunctions.Add(typeof(long), (ValueDataType.SignedInteger, value => value.ToString()));

            _valueFunctions.Add(typeof(float), (ValueDataType.Double, value => value.ToString()));
            _valueFunctions.Add(typeof(double), (ValueDataType.Double, value => value.ToString()));
            _valueFunctions.Add(typeof(decimal), (ValueDataType.Double, value => value.ToString()));

            _valueFunctions.Add(typeof(char), (ValueDataType.Keyword, value => value.ToString()));
            _valueFunctions.Add(typeof(string), (ValueDataType.Keyword, value => value));
            _valueFunctions.Add(typeof(Enum), (ValueDataType.Keyword, value => value.ToString()));
            _valueFunctions.Add(typeof(Guid), (ValueDataType.Keyword, value => value.ToString()));

            _valueFunctions.Add(typeof(DateTime), (ValueDataType.Double, value => ((DateTime)value).ToUniversalTime()
                .ToTimestampMilliseconds().ToString()));

            _valueFunctions.Add(typeof(DateTimeOffset), (ValueDataType.SignedInteger, value => ((DateTimeOffset)value)
                 .ToUnixTimeMilliseconds().ToString()));

            _valueFunctions.Add(typeof(TimeSpan), (ValueDataType.Double, value => ((TimeSpan)value).TotalMilliseconds.ToString()));
        }
    }
}